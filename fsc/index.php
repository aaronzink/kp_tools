<?
	//load templates
	require('templates.php');

	$json = json_decode(file_get_contents("fsc-conf.json"), true);
	ksort($json);
	if($_POST['col_name']) {
		$json[$_POST['col_name']]['name'] = $_POST['col_name'];
		$json[$_POST['col_name']]['exports_dm'] = $_POST['col_exports'];
		$json[$_POST['col_name']]['drives'] = $_POST['col_drives'];
		$json[$_POST['col_name']]['size'] = $_POST['col_size'];
		$json[$_POST['col_name']]['localgroups'] = $_POST['col_localgroups'];
		$json[$_POST['col_name']]['connections'] = $_POST['col_connections'];
		file_put_contents("fsc-conf.json", json_encode($json));
		die("API post successful");
	}
	elseif($_POST['name']) {
		$json[$_POST['name']] = $_POST;
		file_put_contents("fsc-conf.json", json_encode($json));
	}
	
	if($_GET['deletename']) {
		unset($json[$_GET['deletename']]);
		file_put_contents("fsc-conf.json", json_encode($json));
	}
	elseif($_GET['loadname']) {
		$data = $json[$_GET['loadname']];
	}
	else {
		$data = $_POST;
	}

	$name = $data['name'];
	$domain = $data['domain'];
	$oldip = $data['oldip'];
	$drives = $data['drives'];
	$basevolgroup = $data['basevolgroup'];
	$target = $data['target'];
	$metadata = $data['metadata'];
	$vip = $data['vip'];
	$size = $data['size'];
	$namespace = $data['namespace'];
	$exports_dm = $data['exports_dm'];
	$sv = $data['sv'];
	$sv_dst = $data['sv_dst'];
	$sv_src = $data['sv_src'];
	$sv_ret_src = $data['sv_ret_src'];
	$sv_ret_dst = $data['sv_ret_dst'];
	$sv_sched = $data['sv_sched'];
	$localgroups = $data['localgroups'];
	$connections = $data['connections'];

	$schedule = "auto_weekday";
	$template = $template_header;
	$volgroup = $basevolgroup;
	foreach($drives as $drive) {
		if($drive) {
			$template .= str_replace("{VOLGROUP}", $volgroup, str_replace("{DRIVE}", strtolower($drive), $template_volume));
			$exports .= "    export $namespace \"/mv_" . strtolower($name) . "_" . strtolower($drive) . "/\" as \"$name" . "_$drive\$\"\n";
			$netapp_drives .= str_replace("{DRIVE}", strtoupper($drive), $template_netapp_drive);
			if($sv) $netapp_svdrives .= str_replace("{DRIVE}", strtoupper($drive), $template_netapp_svdrive);
			$volgroup++;
		}
	}

	$netapp_drives .= $template_netapp_drive_footer;
	$template .= $template_exports;

	$exportlines = explode("\n", $exports_dm);
	$numexports = 0;
	foreach($exportlines as $line) {
		$parts = null;
		$pathparts = null;
		$exportpath = null;
		$exportname = null;
		$exportdesc = null;

		$parts = preg_split("/[,]*\\\"([^\\\"]+)\\\"[,]*|[,]+/", $line, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		if(count($parts) >= 2) {
			$numexports++;
			$exportname = $parts[0];
			$exportdesc = substr(str_replace("\"", "", $parts[2]), 0, 64);
		
			$pathparts = explode(":", $parts[1]);
			if(in_array(strtoupper($pathparts[0]), $drives) || in_array(strtolower($pathparts[0]), $drives)) {
				$exportpath = strtolower($pathparts[0]) . $pathparts[1];
				$exportpath = str_replace("\\", "/", $exportpath);

				if(strlen($exportdesc) > 2) {
					$exports .= "    export $namespace \"/mv_{NAME}_$exportpath\" as \"$exportname\" description \"$exportdesc\"\n";
				} else {
					$exports .= "    export $namespace \"/mv_{NAME}_$exportpath\" as \"$exportname\"\n";
				}
			}
		}
	}

	$sv_size = $size * 1.5;

	$template .= $exports;
	$template .= $template_footer;
	$template = str_replace("{NAME}", strtolower($name), $template);
	$template = str_replace("{DOMAIN}", strtolower($domain), $template);
	$template = str_replace("{OLDIP}", $oldip, $template);
	$template = str_replace("{TARGET}", strtolower($target), $template);
	$template = str_replace("{TARGET_SHORT}", strtolower($target_short), $template);
	$template = str_replace("{VIP}", strtolower($vip), $template);
	$template = str_replace("{METADATA}", $metadata, $template);
	$template = str_replace("{SCHEDULE}", $schedule, $template);
	$template = str_replace("{NAMESPACE}", $namespace, $template);
	
	$target_short = explode(".", $target);
	$target_short = $target_short[0];
	$netapp = $template_netapp;
	$netapp .= $netapp_drives;
	if($sv) $netapp .= $template_netapp_sv;
	if($sv) $netapp .= $netapp_svdrives;
	$netapp = str_replace("{NAME}", strtolower($name), $netapp);
	$netapp = str_replace("{TARGET_SHORT}", strtolower($target_short), $netapp);
	$netapp = str_replace("{SIZE}", $size, $netapp);
	$netapp = str_replace("{SV_SRC}", $sv_src, $netapp);
	$netapp = str_replace("{SV_DST}", $sv_dst, $netapp);
	$netapp = str_replace("{SV_RET_SRC}", $sv_ret_src, $netapp);
	$netapp = str_replace("{SV_RET_DST}", $sv_ret_dst, $netapp);
	$netapp = str_replace("{SV_SCHED}", $sv_sched, $netapp);
	$netapp = str_replace("{SV_SIZE}", $sv_size, $netapp);

	//$template = str_replace(" ", "&nbsp;", $template);

	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>

<html>
<head>
	<title>KP FSC: ARX and NetApp configuration generator</title>
	<style type="text/css">
		body {
			font-family: Calibri, Tahoma;
			font-size: 12px;
		}
		td {
			font-family: Calibri, Tahoma;
			font-size: 12px;
			text-align: left;
			vertical-align: top;
		}
		th {
			font-size: 14px;
		}
		input {
			width: 150px;
		}
		textarea {
			font-family: Consolas;
      white-space: nowrap;
      overflow:    scroll;
		}
		li {
			margin-left: -25px;
		}
		a {
			  text-decoration: none;
		}
		a:hover {
			  text-decoration: underline;
		}
		.wrapper {
			width: 900px;
			padding: 5px;
			border: solid 1px black;
		}
	</style>
<head>
<body>

<div style="float: right; margin: 10px; padding: 10px; border: solid 1px black; width: 150px;">
<b><a href="index.php">Build</a> | <a href="enable.php">Enable</a></b>
<p>Load previous configs:
<p>
<?
foreach($json as $k => $v) {
	if(substr($k, 0, 1) != $lastletter) {
		print("<br>");
	}
	print("<a style='color: red; font-weight: bold;' href='index.php?deletename=$k'>x</a> <a href='index.php?loadname=$k'>$k</a><br>\n");
	$lastletter = substr($k, 0, 1);
}
?>
<p>New configs saved by name
<hr>
<p><b>Usage instructions</b>
<ol><li>Run <a href="fsc-collect.ps1">fsc-collect.ps1</a> against the targeted server
<li>Return to this page and load the config above.  Note that fields with a * are filled in.
<li>Fill in the remaining information as needed.  All information is required except snapvault.
<li>Click generate configs to generate the ARX and NetApp configs.  Paste command text into devices to configure or update as needed.
<li>fsc-collect.ps1 can be re-run as needed to update information
</ol>
</div>

<h1>KP FSC configuration generator - Build scripts</h1>

<form method="POST" action="index.php">
<div class="wrapper">
<table>
	<tr>
		<td>
			<table>
				<tr><th colspan="2">Old server information</th></tr>
				<tr><td>Old server name:</td><td><input name="name" value="<?=$name?>"></td></tr>
				<tr><td>Old server domain:</td><td><input name="domain" value="<?=$domain?>"></td></tr>
				<tr><td>Old server new IP:</td><td><input name="oldip" value="<?=$oldip?>"></td></tr>
				<tr><td>Drive letters: *</td>
					<td>
						<input name="drives[0]" value="<?=$drives[0]?>" style="width: 20px;">
						<input name="drives[1]" value="<?=$drives[1]?>" style="width: 20px;">
						<input name="drives[2]" value="<?=$drives[2]?>" style="width: 20px;">
						<input name="drives[3]" value="<?=$drives[3]?>" style="width: 20px;">
						<input name="drives[4]" value="<?=$drives[4]?>" style="width: 20px;">
						<input name="drives[5]" value="<?=$drives[5]?>" style="width: 20px;">
				</td></tr>
				<tr><td>Total volume size: *<br>(rounded to nearest 100g)</td><td><input name="size" value="<?=$size?>"></td></tr>
			</table>
		</td>
		<td>
			<table>
				<tr><th colspan="2">Target and other information</th></tr>
				<tr><td>Target filer:<br>(full name as in the ARX)</td><td><input name="target" value="<?=$target?>"></td></tr>
				<tr><td>Target global server:<br>(full name as in the ARX)</td><td><input name="vip" value="<?=$vip?>"></td></tr>
				<tr><td>Drive 1 vol group #:<br>(addl. drives incremented)</td><td><input name="basevolgroup" value="<?=$basevolgroup?>"></td></tr>
				<tr><td>Namespace:</td><td><input name="namespace" value="<?=$namespace?>"></td></tr>
				<tr><td>Metadata string:<br>(full name as in the ARX)</td><td><input name="metadata" value="<?=$metadata?>"></td></tr>
			</table>
		</td>
		<td>
			<table>
				<tr><th colspan="2">Snapvault</th></tr>
				<tr><td>Use snapvault?:</td><td><input type="checkbox" name="sv" value="1"	<? if($sv) print(" checked "); ?>></td></tr> 
				<tr><td>Snapvault source:<br>(physical filer)</td><td><input name="sv_src" value="<?=$sv_src?>"></td></tr>
				<tr><td>Snapvault destination:<br>(physical filer)</td><td><input name="sv_dst" value="<?=$sv_dst?>"></td></tr>
				<tr><td>Snapvault source retention:<br>(# of days)</td><td><input name="sv_ret_src" value="<?=$sv_ret_src?>"></td></tr>
				<tr><td>Snapvault destination retention:<br>(# of days)</td><td><input name="sv_ret_dst" value="<?=$sv_ret_dst?>"></td></tr>
				<tr><td>Snapvault schedule:<br>(hour of day (0-23))</td><td><input name="sv_sched" value="<?=$sv_sched?>"></td></tr>
			</table>
		</td>
	</tr>
</table>
<hr>
Information from scan (read only)
<table style="width: 100%">
	<tr>
		<td style="width: 30%">
			<p>Shares
			<textarea style="width: 100%; height: 150px" name="exports_dm" readonly="true"><?=$exports_dm?></textarea>
		</td>
		<td style="width: 40%">
			<p>Connected users at time of scan
			<textarea style="width: 100%; height: 150px" name="connections" readonly="true"><?=$connections?></textarea>
		</td>
		<td style="width: 30%">
			<p>Local groups and other
			<textarea style="width: 100%; height: 150px" name="localgroups" readonly="true"><?=$localgroups?></textarea>
		</td>
	</tr>
</table
<p>* Values will be overridden by <a href="fsc-collect.ps1">fsc-collect.ps1</a>.  Running this script is highly recommended as a starting point for building the config.
<p><input type="submit" value="Generate configs">
</form>
</div>
<?
if($name && $domain && $oldip && $size && $vip && $namespace && $target && $volgroup && $metadata) {
?>
<h3>ARX Configuration Output - (Number of exports: <?=$numexports?>)</h3>
<textarea style="width: 900px; height: 400px;"><?=$template?></textarea>
<h3>NetApp Configuration Output</h3>
<textarea style="width: 900px; height: 400px;"><?=$netapp?></textarea>
<?
}
else {
	print("<i>At minimum, old server name, domain, IP, size, target filer, global server, vol group, namespace, and metadata are required to generate a configuration</i>");
}

?>
</body>
</html>
