<?
	//load templates
	require('templates.php');

	$json = json_decode(file_get_contents("fsc-conf.json"), true);
	if($_POST['servers']) {
		$servers = $_POST['servers'];
	}
	
	$en = "; Enable script for " . implode(" ", $_POST['servers']) . "\n\n";
	$en .= "; NOTE: be sure to be in global and the appropriate namespace context for each server\n";

	$en .= "; Step 1 - Enable volumes and shares\n";
	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$en .= "    volume /mv_" . $server . "_" . strtolower($drive) . "\n";
				$en .= "      enable\n";
				$en .= "      share sh_" . $server . "_" . strtolower($drive) . "_source\n";
				$en .= "        enable\n";
				$en .= "        exit\n";
				$en .= "      exit\n\n";
			}
		}
	}
	
	$en .= "; Step 2 - Enable destination volume after source import\n";
	
	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$en .= "    volume /mv_" . $server . "_" . strtolower($drive) . "\n";
				$en .= "      share sh_" . $server . "_" . strtolower($drive) . "_fas\n";
				$en .= "        enable\n";
				$en .= "        exit\n";
				$en .= "      exit\n\n";
			}
		}
	}
	
	$en .= "; Step 3 - Enable 120-day place rule\n";
	
	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$en .= "    volume /mv_" . $server . "_" . strtolower($drive) . "\n";
				$en .= "      place-rule pr_" . $server . "_" . strtolower($drive) . "_120\n";
				$en .= "        enable\n";
				$en .= "        exit\n";
				$en .= "      exit\n\n";
			}
		}
	}
		
	$en .= "; Step 4 - Enable snapshots\n"; 
	
	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$en .= "    volume /mv_" . $server . "_" . strtolower($drive) . "\n";
				$en .= "      snapshot rule snap_" . $server . "_" . strtolower($drive) . "_daily\n";
				$en .= "        enable\n";
				$en .= "        exit\n";
				$en .= "      exit\n\n";
			}
		}
	}
	
	$en .= "; Step 5 - Enable full place rule after 120 is complete or stopped\n"; 
	
	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$en .= "    volume /mv_" . $server . "_" . strtolower($drive) . "\n";
				$en .= "      place-rule pr_" . $server . "_" . strtolower($drive) . "_all\n";
				$en .= "        enable\n";
				$en .= "        exit\n";
				$en .= "      exit\n\n";
			}
		}
	}

	foreach($servers as $server) {
		$drives = $json[$server]['drives'];
		$domain = $json[$server]['domain'];
		$server = strtolower($server);
		foreach($drives as $drive) {
			if($drive) {
				$test .= ".\\test-share.ps1 \\\\" . $server . "." . $domain . "\\". $server . "_" . strtolower($drive) . "$\n";
			}
		}
	}


	$spn = "# setspn commands\n\n";
	foreach($servers as $server) {
		$domain = $json[$server]['domain'];
		$vip = explode(".", $json[$server]['vip']);
		$vip = strtolower($vip[0]);
		$name = strtolower($server);

		$spn .= "setspn -A CIFS/$name $vip\n";
		$spn .= "setspn -A HOST/$name $vip\n"; 
		$spn .= "setspn -A CIFS/$name.$domain $vip\n"; 
		$spn .= "setspn -A HOST/$name.$domain $vip\n"; 
		$spn .= "setspn -A CIFS/$vip.$domain $vip\n"; 
		$spn .= "setspn -A HOST/$vip.$domain $vip\n\n"; 
	}

	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>

<html>
<head>
	<title>KP FSC: ARX and NetApp configuration generator - enable scripts</title>
	<style type="text/css">
		body {
			font-family: Calibri, Tahoma;
			font-size: 12px;
		}
		textarea {
			font-family: Consolas;
		}
		li {
			margin-left: -25px;
		}
		a {
			text-decoration: none;
		}
		a:hover {
			text-decoration: underline;
		}
		.wrapper {
			width: 900px;
			padding: 5px;
			border: solid 1px black;
		}
	</style>
<head>
<body>
<h1>KP FSC configuration generator - Enable scripts</h1>
<div style="float: right; margin: 10px; padding: 10px; border: solid 1px black; width: 150px;">
<b><a href="index.php">Build</a> | <a href="enable.php">Enable</a></b><br>
<p>Select servers to generate scripts for:</p>
<form method="POST" action="enable.php">
<?
foreach($json as $k => $v) {
	print("<input type='checkbox' name='servers[]' value='$k'>$k<br>");
}
?>
<input type="submit" value="Submit">
</form>
</div>
<h3>ARX enable output</h3>
<textarea style="width: 900px; height: 400px;"><?=$en?></textarea>
</body>
<h3>SetSPN Syntax</h3>
<textarea style="width: 900px; height: 300px;"><?=$spn?></textarea>
<h3>Test scripts</h3>
<textarea style="width: 900px; height: 300px;"><?=$test?></textarea>
</body>
</html>
