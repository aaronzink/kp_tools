# version 2.0.1

function Send-WebContent {
[CmdletBinding()]
Param(
	[Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Mandatory=$true, Position=0)]	
	[String]$Url,
	[Parameter(ParameterSetName='Data')]
	[HashTable]$Data,
	[Parameter(ParameterSetName='Content')]
	[String]$Content,
	[TimeSpan]$Timeout = [TimeSpan]::FromMinutes(1),
	[Management.Automation.PSCredential]$Credential,
	[String]$ContentType
)	
	try{
		$req = [Net.WebRequest]::Create($Url)
		$req.Method = "POST"
		$req.Timeout = $Timeout.TotalMilliseconds
		if ($Credential){
			$ntwCred = $Credential.GetNetworkCredential()
			$auth = "Basic " + [Convert]::ToBase64String([Text.Encoding]::Default.GetBytes($ntwCred.UserName + ":" + $ntwCred.Password))
			$req.Headers.Add("Authorization", $auth)
			$req.Credentials = $ntwCred
			$req.PreAuthenticate = $true
		}

		if ($ContentType -ne ""){
			$req.ContentType = $ContentType
		}

		switch($PSCmdlet.ParameterSetName) {
			Content { 
				$reqStream = $req.GetRequestStream()
				$reqBody = [Text.Encoding]::Default.GetBytes($Content)
				$reqStream.Write($reqBody, 0, $reqBody.Length)
			}
			Data {
				Add-Type -AssemblyName System.Web
				$formData = [Web.HttpUtility]::ParseQueryString("")
				foreach($key in $Data.Keys){
					$formData.Add($key, $Data[$key])
				}
				$reqBody = [Text.Encoding]::Default.GetBytes($formData.ToString())
			
				$req.ContentType = "application/x-www-form-urlencoded"
				$reqStream = $req.GetRequestStream()
				$reqStream.Write($reqBody, 0, $reqBody.Length)
			}
		}
			
		$reqStream.Close()
		
		$Method = $req.Method
		Write-Verbose "Execute $Method request"
		foreach($header in $req.Headers.Keys){
			Write-Verbose ("$header : " + $req.Headers[$header])
		}
				
		$resp = $req.GetResponse()
		$respStream = $resp.GetResponseStream()
		$respReader = (New-Object IO.StreamReader($respStream))
		$respReader.ReadToEnd()
	}
	catch [Net.WebException]{
		if ($_.Exception -ne $null -and $_.Exception.Response -ne $null) {
			$errorResult = $_.Exception.Response.GetResponseStream()
			$errorText = (New-Object IO.StreamReader($errorResult)).ReadToEnd()
			Write-Error "The remote server response: $errorText"
		}
		throw $_
	}
}

write-host "fsc-collect.ps1 - Only to be used in conjunction with KP FSC configurator."
if($args.length -lt 1) {
	write-host "Usage: .\fsc-collect.ps1 server"
	exit
}
$computer = $args[0]
write-host "Collecting information on" $computer "..."

$drives = @()
$col_localgroups = @()

echo "Drives..."
$col_localgroups += "Drives"
$col_localgroups += "------"

$total = 0
$disks = Get-WMIObject -query "select * from Win32_LogicalDisk where MediaType = 12" -computername $computer
foreach($disk in $disks) {
	if($disk.Name -ne "C:") {
		$col_drives += $disk.Name.substring(0,1).toUpper()
		$total += $disk.Size - $disk.FreeSpace
		$used = [math]::round(($disk.Size - $disk.FreeSpace) / 1024 / 1024 / 1024, 2)
		$col_localgroups += $disk.Name + " $used GB used"
	}
}
$col_size = [math]::round(($total * 1.25 / 1024 / 1024 / 1024)/100, 0) * 100

echo "Shares..."
$shares = Get-WMIObject -query "Select * from Win32_Share where Type = 0" -computername $computer
$col_exports = $shares | select name, path, description | ConvertTo-Csv -NoTypeInformation | select -skip 1 | sort | out-string
$col_localgroups += $shares.count.toString() + " shares on source"

echo "Connected users..."
$connections = Get-WMIObject -query "Select * from Win32_ServerConnection" -computername $computer
$col_connections = $connections | select __SERVER, UserName, ComputerName, ShareName | ConvertTo-Csv -NoTypeInformation | select -skip 1 | sort | out-string
$col_localgroups += $connections.count.toString() + " connected users"

echo "Local groups..."
$compObj = [ADSI]("WinNT://" + $computer + ",computer")

$localGroups = $compObj.psbase.children | where {$_.psbase.schemaclassname -eq "group"}
$skipGroups = @("Administrators", "Backup Operators", "Distributed COM Users", "Guests", "Network Configuration Operators", "Performance Log Users", "Performance Monitor Users", "Power Users", "Print Operators", "Remote Desktop Users", "Replicator", "Users", "HelpServicesGroup", "TelnetClients", "Tivoli_Admin_Privileges")

$col_localgroups += ""
$col_localgroups += "Domain group : Missing users"
$col_localgroups += "----------------------------"

$numGroups = $localGroups.count
$i = 0;

foreach ($localGroup in $localGroups.psbase.syncroot) {
	$groupName = $localGroup.name
	write-progress -activity "Resolving $numGroups local groups..." -status "Processing $groupName" -percentcomplete (($i / $numGroups)*100)
	$i++
	if(!($skipGroups -match $groupName)) {
		$groupWarn = $false
		$memberObj = @($localGroup.psbase.invoke("Members"))
		$memberObj | foreach {
			$uid = $null
			$member = $null
			$userGroups = $null
			$member = $_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)
			try {
				$user = get-QADUser -Enabled $member
				$userGroups = get-QADMemberOf -indirect $user
				$uid = $user.sAMAccountName
			}
			catch {}
			$isFound = $false
			foreach($userGroup in $userGroups) {
				if($userGroup.Name -eq $groupName) {
					$isFound = $true
				}
			}
			if(!$isFound) {
				if($uid) {
					$out = $domain + "\" + $groupName + " : " + $domain + "\" + $uid
					$col_localgroups += $out
				}
				elseif(!$groupWarn) {
					$groupWarn = $true
					$out = $domain + "\" + $groupName + " : Legacy group(s) found, check manually"
					$col_localgroups += $out
				}
			}
		}
	}
}
$col_localgroups = $col_localgroups -join ("`n")

echo "Posting data to FSC configuration API..."
$postdata = @{"col_name" = $computer; "col_domain" = "testdomain"; "col_exports" = $col_exports; "col_drives" = $col_drives; "col_size" = $col_size; "col_connections" = $col_connections; "col_localgroups" = $col_localgroups }
$URL = "http://nzxpvm2083.dcld.nndc.kp.org/kp-fsc/index.php"

$cred = New-Object System.Management.Automation.PSCredential -ArgumentList @("kp",(ConvertTo-SecureString -String "T34kaiser" -AsPlainText -Force))

Send-WebContent $URL -Credential $cred -data $postdata

write-host "Done"

