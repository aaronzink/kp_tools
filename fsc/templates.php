<?
	$template_header = <<<EOD
no terminal confirmation
global
  external-filer {NAME}old.{DOMAIN}
    cifs-port 445
    ip address {OLDIP}
    spn {NAME}old@cs.msds.kp.org
    exit

  namespace {NAMESPACE}

EOD;

	$template_volume = <<<EOD
    volume /mv_{NAME}_{DRIVE}
      ;cifs access-based-enum auto-enable
      modify
      reimport-modify
      policy migrate-method direct
      snapshot directory display all-exports
      snapshot directory display all-exports hidden
      snapshot privileged-access
      auto sync files
      metadata share {METADATA}
      no compressed-files
      named-streams
      persistent-acls
      sparse-files
      unicode-on-disk
      auto reserve files
      share sh_{NAME}_{DRIVE}_fas
        freespace ignore
        import priority 500
        import sync-attributes
        no import rename-directories
        no import rename-files
        filer {TARGET} cifs {NAME}_{DRIVE}$
        exit

      share sh_{NAME}_{DRIVE}_source
        import priority 1
        migrate retain-files
        no import rename-directories
        no import rename-files
        filer {NAME}old.{DOMAIN} cifs ARX_{DRIVE}$
        exit

      snapshot rule snap_{NAME}_{DRIVE}_daily
        schedule daily
        retain 30
        exit

      place-rule pr_{NAME}_{DRIVE}_120
        schedule {SCHEDULE}
        report pr_{NAME}_{DRIVE}_120 verbose
        source share sh_{NAME}_{DRIVE}_source
        from fileset new_files_120
        target share sh_{NAME}_{DRIVE}_fas
        exit

      place-rule pr_{NAME}_{DRIVE}_all
        schedule {SCHEDULE}
        report pr_{NAME}_{DRIVE}_all verbose
        source share sh_{NAME}_{DRIVE}_source
        target share sh_{NAME}_{DRIVE}_fas
        exit

      volume-group {VOLGROUP}
      exit


EOD;

  $template_exports = <<<EOD
  exit

  cifs {VIP}
    signatures
    browsing {NAMESPACE}

EOD;

  $template_footer = <<<EOD
  exit

EOD;

	$template_netapp = <<<EOD
# Commands for filer running {TARGET_SHORT}
vol create {NAME} aggr01 {SIZE}g
snap reserve {NAME} 10
snap sched {NAME} 0 0 0
qtree security /vol/{NAME} ntfs
sis config -s sun-sat@0 /vol/{NAME}
sis on /vol/{NAME}
vfiler add {TARGET_SHORT} /vol/{NAME}
vfiler context {TARGET_SHORT}

EOD;

	$template_netapp_drive = <<<EOD
qtree create /vol/{NAME}/{DRIVE}_DRIVE
qtree security /vol/{NAME}/{DRIVE}_DRIVE ntfs
cifs shares -add {NAME}_{DRIVE}$ /vol/{NAME}/{DRIVE}_DRIVE
cifs shares -change {NAME}_{DRIVE}$ -accessbasedenum
cifs access {NAME}_{DRIVE}$ "authenticated users" "change"
cifs access -delete {NAME}_{DRIVE}$ "everyone" "full control"

EOD;

	$template_netapp_drive_footer = <<<EOD
vfiler context vfiler0

EOD;

	$template_netapp_sv = <<<EOD

# Snapvault commands for source filer: {SV_SRC}
vfiler context vfiler0
snapvault snap sched {NAME} sv_daily {SV_RET_SRC}@{SV_SCHED}

# Snapvault commands for destination filer: {SV_DST}
vol create {NAME}_bkup aggr02 {SV_SIZE}g
snap reserve {NAME}_bkup 0
snap sched {NAME}_bkup 0 0 0
snapvault snap sched -x {NAME}_bkup sv_daily {SV_RET_DST}@{SV_SCHED}
cifs shares -add {NAME}_bkup$ /vol/{NAME}_bkup
cifs access {NAME}_bkup$ "authenticated users" "read"
cifs access -delete {NAME}_bkup$ "everyone" "full control"

EOD;

	$template_netapp_svdrive = <<<EOD
snapvault start -S {SV_SRC}:/vol/{NAME}/{DRIVE}_DRIVE {SV_DST}:/vol/{NAME}_bkup/{DRIVE}_DRIVE

EOD;


//End of templates
?>
