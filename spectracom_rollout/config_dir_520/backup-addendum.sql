# additional SQL

DROP TABLE IF EXISTS `remote_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL,
  `events` tinyint(1) NOT NULL,
  `auth` tinyint(1) NOT NULL,
  `alarms` tinyint(1) NOT NULL,
  `timing` tinyint(1) NOT NULL,
  `qual` tinyint(1) NOT NULL,
  `osc` tinyint(1) NOT NULL,
  `journal` tinyint(1) NOT NULL,
  `update` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `remote_servers` WRITE;
/*!40000 ALTER TABLE `remote_servers` DISABLE KEYS */;
#syslog_servers#
/*!40000 ALTER TABLE `remote_servers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
