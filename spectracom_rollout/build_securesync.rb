#!/usr/bin/ruby

require 'rubygems'
require 'optparse'
require 'ipaddress'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "SecureSync Build v1.6, developed by Trace3 for Kaiser Permanente.\nUsage: build_securesync.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Image options:"
  opts.on("-b", "--base_image FILE", "Base image") {|x| args[:base_image] = x }
  opts.on("-c", "--config DIRECTORY", "Configuration directory to apply") {|x| args[:config_dir] = x }
  opts.separator "Base options:"
  opts.on("-n", "--hostname NAME", "Hostname") {|x| args[:hostname] = x }
  opts.on("-m", "--mgmt_ip IP", "Management IP/mask") {|x| args[:mgmt_ip] = x }
  opts.on("-t", "--trans_ip IP", "Transaction IP/mask") {|x| args[:trans_ip] = x }
  opts.on("-d", "--dns_servers SERVER1,SERVER2,...", "DNS server list, comma-separated") {|x| args[:dns_servers] = x }
  opts.on("-o", "--snmp_community COMMUNITY", "SNMP community string") {|x| args[:snmp_community] = x }
  opts.on("-g", "--snmp_servers [SERVER1,SERVER2,...]", "SNMP trap targets, comma-separated") {|x| args[:snmp_servers] = x }
  opts.on("-y", "--syslog_servers [SERVER1,SERVER2,...]", "Syslog targets, comma-separated") {|x| args[:syslog_servers] = x }
  opts.separator "NTP options:"
  opts.on("-s", "--ntp_servers [SERVER1,SERVER2,...]", "NTP server list, comma-separated") {|x| args[:ntp_servers] = x }
  opts.on("-p", "--ntp_peers [SERVER1,SERVER2,...]", "NTP peer list, comma-separated") {|x| args[:ntp_peers] = x }
  opts.on("-1", "--is_stratum1", "Set if this server is Stratum 1") {|x| args[:is_stratum1] = true }
end.parse!

# Parse options
args[:mgmt_mask] = IPAddress(args[:mgmt_ip]).netmask
args[:mgmt_gw] = IPAddress.parse(args[:mgmt_ip]).first.address
args[:mgmt_ip] = IPAddress.parse(args[:mgmt_ip]).address
args[:trans_mask] = IPAddress.parse(args[:trans_ip]).netmask
args[:trans_gw] = IPAddress.parse(args[:trans_ip]).first.address
args[:trans_ip] = IPAddress.parse(args[:trans_ip]).address
args[:domain] = args[:hostname].split('.').drop(1).join('.')
args[:shortname] = args[:hostname].split('.')[0]

# Multi-line replacements
args[:dns_servers] = args[:dns_servers].split(',').map {|x| "nameserver #{x}"}.join("\\n")
args[:ntp_peers]   = args[:ntp_peers].split(',').map {|x| "peer #{x} key 2 minpoll 3 maxpoll 3"}.join("\\n") if args[:ntp_peers]
args[:ntp_servers] = args[:ntp_servers].split(',').map {|x| "server #{x} key 2 minpoll 3 maxpoll 3"}.join("\\n") if args[:ntp_servers]
args[:snmp_servers] = args[:snmp_servers].split(',').map {|x| "trapsess -v 2c -c #{args[:snmp_community]} udp:#{x}:162"}.join("\\n") if args[:snmp_servers]
args[:syslog_servers] = args[:syslog_servers].split(',').map {|x| "INSERT INTO remote_servers VALUES (1,'#{x}',1,1,1,1,1,1,1,1,1);"}.join("\\n") if args[:syslog_servers]
args[:is_stratum1] = "fudge 127.127.22.0 stratum 0\\nserver 127.127.45.0 minpoll 4 prefer\\nrestrict -6 default noquery nomodify" if args[:is_stratum1]


# Sanity checking
abort("ERROR: Base image does not exist") unless File.exist?(args[:base_image])
abort("ERROR: Config directory does not exist") unless File.directory?(args[:config_dir])

# Build configuration
puts "Reading image and configuration..."
`rm -Rf builds/#{args[:hostname]}.conf builds/#{args[:hostname]}`
`mkdir ss_build_temp`
`tar -xf #{args[:base_image]} -C ss_build_temp`
`cp -Rv #{args[:config_dir]}/* ss_build_temp`

puts "Building configuration..."
args.each do |k, v|
  v.delete!('/')
  `LC_ALL=C find ss_build_temp -type f -exec gsed -i "s/##{k.to_s}#/#{v}/g" "{}" \\;`
end

# SQL replacements
`tar -xf ss_build_temp/srv/mysql/backup.sql.tar.gz -C ss_build_temp/srv/mysql`
`cat ss_build_temp/backup-addendum.sql >> ss_build_temp/srv/mysql/backup.sql`
`tar -cf ss_build_temp/srv/mysql/backup.sql.tar.gz -C ss_build_temp/srv/mysql backup.sql`
`rm ss_build_temp/srv/mysql/backup.sql`
`rm ss_build_temp/backup-addendum.sql`

# Final build
`tar -zcf #{args[:hostname]}.conf -C ss_build_temp/ . `
`rm -Rf ss_build_temp`
`mv #{args[:hostname]}.conf builds/`

puts "Done, output file is #{args[:hostname]}.conf"
