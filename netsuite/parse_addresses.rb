#/usr/bin/env ruby
# Example parsing script.  Use these regexes as an example - Aaron Zink - azink@trace3.com

# Require Prity Print to show array.
require "pp"

# Read file and split into an array
data = File.read("kp addresses.txt").split("\n")

# Put all parsed data into a new array
parsed = []

# Parse each line uusing regexes
data.each do |line|
  parsed << {
    :original => line,
    :company  => line[/^(.+)\s+\d+.+\w\w\s+\d{5}(-\d{4})?/i, 1],
    :address  => line[/\s+(\d+.+\w\w\s+\d{5}(-\d{4})?)/i, 1],
    :state    => line[/(\w\w).?\s+\d{5}(-\d{4})?/i, 1],
    :zip      => line[/\d+.+\w\w\s+(\d{5}(-\d{4})?)/i, 1],
    :contact  => line[/(\w\w).?\s\d{5}(-\d{4})?\s+(attn)?:?\s+(.+)$/i, 4]
  }
end

# print array
pp parsed
