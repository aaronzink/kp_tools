#/usr/bin/env ruby
# Example parsing script.  Use these regexes as an example - Aaron Zink - azink@trace3.com

# Require Prity Print to show array.
require "pp"

# Read file and split into an array
data = File.read("kp serials.txt").split("\n")

# Put all parsed data into a new array
parsed = []

# Parse each line uusing regexes
data.each do |line|
  parsed << {
    :original => line,
    :serial  => line[/(serial number(s?)|s\/?n)\s?:?\s?(\S+)/i, 3]
  }
end

# print array
pp parsed
