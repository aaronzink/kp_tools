#!/usr/bin/env ruby
# Simple HTTP test script - Aaron Zink - azink@trace3.com

responses = []
i = 0
puts "Start time: #{Time.new}"
print "Testing, use Ctrl-C to stop ...  "
begin
  while i > -1 do
    i += 1
    data = `curl -ksw "%{response_code} %{time_total}s" #{ARGV[0]}`
    responses << {
      response_text: data[/Server: (\S+)</, 1],
      response_code: data.split("\n").last[/^\d+/],
      response_time: data.split("\n").last[/\s(\S+)$/, 1]
    }
    print "\b" if i % 10 == 0
    print "/" if i % 30 == 0
    print "-" if i % 30 == 10
    print "\\" if i % 30 == 20
    sleep 1 if responses.last[:response_code] == "000"
  end
rescue Interrupt => e
  avg_time = responses.map {|x| x[:response_time].to_f}.reduce(:+) / responses.count
  response_codes = responses.map {|x| [x[:response_code], responses.count {|y| y[:response_code] == x[:response_code]}].join(" -> ") }.uniq.join("; ")
  response_texts = responses.map {|x| [x[:response_text], responses.count {|y| y[:response_text] == x[:response_text]}].join(" -> ") }.uniq.join("; ")
  puts 
  puts "End time:   #{Time.new}"
  puts "------------"
  puts "URL:        #{ARGV[0]}"
  puts "Requests:   #{responses.count}"
  puts "Responses:  #{response_codes}"
  puts "Servers:    #{response_texts}"
  puts "Avg Time:   #{avg_time}"
end
