#!/bin/bash

# v6 to add CNF routes

if [[ $1 == "s" ]]
  then profile="KP Silver Spring"
elif [[ $1 == "c" ]]
  then profile="KP Corona"
else
  profile="KP Walnut Creek"
fi

gw=$(netstat -rn | egrep 'default.*(\d+\.){3}' | head | awk '{ print $2 }')

scutil --nc start "$profile"

echo "Waiting for $profile VPN to connect... ^C to cancel"
t=1
while (($t == 1)); do
  scutil --nc list | grep "Connected.*KP" > /dev/null
  t=$?
  sleep 2
done
echo "Connected, fixing routes"
tun=$(netstat -rn | grep "default.*tun" | head | awk '{ print $6 }')
sudo route add -net 172.16.0.0/12 -interface $tun
sudo route add -net 10.0.32.0/22 -interface $tun
sudo route add -net 10.15.0.0/16 -interface $tun
sudo route change default $gw
echo "Done"
